package main


import "fmt"
import "flag"
	
const VERSION = "1.0"

func main() {
	VERSION := flag.Bool("version", true, "Version of the file")

	flag.Parse()

	fmt.Println("vous êtes sur la version:", *VERSION)
}

